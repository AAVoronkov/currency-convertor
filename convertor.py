from bs4 import BeautifulSoup
from decimal import Decimal, ROUND_UP
import requests


print(
    '=====================================\n'
    'Добро пожаловать в конвертор валют\n'
    '=====================================\n'
    'Доступные валюты:\n',
    'Австралийский доллар -- AUD\n', 'Азербайджанский манат -- AZN\n', 
    'Английский фунт стерлингов (СК) -- GBP\n',	'Армянский драм -- AMD\n', 
    'Белорусский рубль -- BYN\n', 'Болгарский лев -- BGN\n', 'Бразильский реал -- BRL\n', 
    'Венгерский форинт -- HUF\n', 'Гонконгский доллар -- HKD\n', 'Датская крона -- DKK\n', 
    'Доллар США -- USD\n', 'Евро -- EUR\n',	'Индийская рупия -- INR\n', 
    'Казахстанский тенге -- KZT\n', 'Канадский доллар -- CAD\n', 'Киргизский сом -- KGS\n', 
    'Китайский юань -- CNY\n', 'Корейская вона -- KRW\n', 'Молдавский лей -- MDL\n', 
    'Норвежская крона -- NOK\n', 'Польский злотый -- PLN\n', 'Румынский лей -- RON\n', 
    'СДР (специальные права заимствования) -- XDR\n', 'Сингапурский доллар -- SGD\n', 
    'Таджикских сомони -- TJS\n', 'Турецкая лира -- TRY\n', 'Туркменский манат (новый) -- TMT\n', 
    'Узбекский сум -- UZS\n', 'Украинская гривна -- UAH\n', 'Чешская крона -- CZK\n', 
    'Шведская крона -- SEK\n', 'Швейцарский франк -- CHF\n', 
    'Южноафриканский рэнд -- ZAR\n', 'Японская йена -- JPY\n'
)


# Функция нахождения номинала валюты
def parse_nominal(soup, currency):
    return Decimal(soup.find('CharCode', text=currency).find_next_sibling('Nominal').string)


# Функция нахождения значения валюты
def parse_value(soup, currency):
    return Decimal('.'.join(soup.find('CharCode', text=currency).find_next_sibling('Value').string.split(',')))


# Начальные параметры
amount = int(input('Сумма: '))
cur_from = str(input('Перевести из: ')).upper()
cur_to = str(input('Перевести в: ')).upper()

# Отправляем запрос на сайт Центробанка
url = 'http://www.cbr.ru/scripts/XML_daily.asp'
print('Waiting for response from www.cbr.ru')
resp = requests.get(url)

if resp.status_code == 200:

    soup = BeautifulSoup(resp.content, "xml")

    # Если переводим из рублей в валюту
    if cur_from == 'RUR':
        nominal_to = parse_nominal(soup, cur_to)
        value_to = parse_value(soup, cur_to)

        result = amount * nominal_to / value_to
    
    # Если переводим валюту в рубли
    elif cur_to == 'RUR':
        nominal_from = parse_nominal(soup, cur_from)
        value_from = parse_value(soup, cur_from)

        result = amount / nominal_from * value_from

    # Если переводим иностранную валюту в другую иностранную валюту
    else:
        nominal_from = parse_nominal(soup, cur_from)
        value_from = parse_value(soup, cur_from)
        nominal_to = parse_nominal(soup, cur_to)
        value_to = parse_value(soup, cur_to)

        result = amount / nominal_from * value_from * (nominal_to / value_to)

    print('Result:\n', '{amount} {cur_from} = {result} {cur_to}'.format(
        amount=amount,
        cur_from=cur_from,
        result=result.quantize(Decimal('.01'), rounding=ROUND_UP),
        cur_to=cur_to
    ))

else:
    print('ERROR')
